<?php

use App\Acme\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = ["ActionScript","Ajax","AngularJS","Apache","AppleScript","ASP.NET","Bash","C","C#","C++","Coffee","CoffeeScript","ColdFusion","Command","CSS","Delphi","Django","ES6","GLSL","Grunt","Gulp","HAML","Haskell","HTML","iOS","Jade","Java","JavaScript","jQuery","JSX","Less","LUA","MDX","MySQL","Objective","Other","Pascal","Perl","PHP","Plain text","PowerShell","Processing","Progress","Prolog","Pseudocode","Python","Rails","RegExr","Ruby","SASS","Scala","Scheme","SCSS","SmallBASIC","Smarty","SQL","Stylus","SVG","Swift","TypeScript","VHDL","X++","XHTML","XML","Xojo","XSLT"];

        foreach ($tags as $tag) {
            Tag::create(['name' => $tag ]);
        }
    }
}
