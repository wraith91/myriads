<?php

namespace App\Acme;

use App\Acme\Model;

class Snippet extends Model
{
    public function forks()
    {
        return $this->hasMany(Snippet::class, 'forked_id');
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }

    public function originalSnippet()
    {
        return $this->belongsTo(Snippet::class, 'forked_id');
    }

    public function isAFork()
    {
        return !! $this->originalSnippet;
    }
}
