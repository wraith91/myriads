@extends('layouts.master')

@section('content')
    <h1 class="title">New Snippet</h1>

    {!! Form::model($snippet, ['url' => 'snippets']) !!}

        @if ($snippet->id)
            {!! Form::hidden('forked_id', $snippet->id, []) !!}
        @endif

        <div class="field">
            <label class="label">Title</label>
            <div class="control">
                {!! Form::text('title', null, ['placeholder' => 'Title of the snippet','class' => 'input']) !!}
            </div>
        </div>

        <div class="field">
            <label class="label">Body</label>
            <div class="control">
                {!! Form::textarea('body', null, ['placeholder' => 'Content of the snippet','class' => 'textarea']) !!}
            </div>
        </div>

        <div class="field">
            <label class="label">Tag</label>
            <div class="control">
                {{ Form::select('tag_id', $tags, null, ['placeholder' => 'Please choose...', 'class' => 'tags']) }}
            </div>
        </div>

        {!! Form::submit('Publish Snippet', ['class' => 'button is-primary']) !!}

    {!! Form::close() !!}
@endsection

@section('footer')
    <script>
        // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            $('.tags').select2();
        });
    </script>
@endsection
