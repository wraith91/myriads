@extends('layouts.master')

@section('content')
    @if (count($snippets))
        @foreach ($snippets as $snippet)
            <article class="snippet">
                <h4 class="title is-4">
                    <a href="/snippets/{{ $snippet->id }}">{{ $snippet->title }}</a>
                </h4>

                {{-- <pre class="editor"><code>{{ $snippet->body }}</code></pre> --}}
            </article>
        @endforeach
    @endif
@endsection

@section('footer')
    <script>
        var editor = ace.edit("editor");
        editor.setTheme("ace/theme/dawn");
        editor.setOptions({
            maxLines: Infinity
        });
        editor.getSession().setMode("ace/mode/javascript");
    </script>
@endsection
